import assert from 'assert'
import RESTClient from '../src'
import nock from 'nock'
import fetchEverywhere from 'fetch-everywhere'
import {LocalStorage} from 'node-localstorage'
import {LOCALSTORAGE_KEY} from '../src'

var BASE = 'http://localhost:8000'
global.localStorage = new LocalStorage('./.tmp')

describe('api', () => {
  var client

  beforeEach(() => {
    client = new RESTClient(BASE)
  })

  after(() => {
    localStorage._deleteLocation()
  })

  describe('auth', () => {

    it('should handle failed login', () => {
      nock(BASE)
        .post('/token_auth/')
        .reply(400)

      return client.logIn('admin', 'testpass').then(function () {
        throw "This should have failed"
      }).catch(function (err) {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(err.status, 400)
        assert.equal(client.token, null)
        assert.equal(client.user, null)
        assert.equal(stored, null)
      })
    })

    it('should retrieve token', () => {
      nock(BASE)
        .post('/token_auth/')
        .reply(200, {
          token: 'test',
          user: 1
        })

      return client.logIn('admin', 'testpass').then(function () {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(client.token, 'test')
        assert.equal(stored.token, 'test')
        assert.equal(client.user, 1)
        assert.equal(stored.user, 1)
      })
    })

    it('should retrieve user', () => {
      nock(BASE)
        .post('/token_auth/')
        .reply(200, {
          user: 1
        })

      return client.logIn('admin', 'testpass').then(function () {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(client.user, 1)
        assert.equal(stored.user, 1)
      })
    })

    it('should allow user to set auth path', () => {
      let customClient = new RESTClient(BASE, {
        AUTH_PATH: '/custom_auth_path/'
      })

      nock(BASE)
        .post('/custom_auth_path/')
        .reply(200, {
          token: 'test',
          user: 1
        })

      return customClient.logIn('admin', 'testpass').then(() => {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(customClient.token, 'test')
        assert.equal(stored.token, 'test')
        assert.equal(customClient.user, 1)
        assert.equal(stored.user, 1)
      })
    })

    it('should allow user to set logout path', () => {
      let customClient = new RESTClient(BASE, {
        LOGOUT_PATH: '/custom_logout_path/'
      })

      customClient.token = 'test'
      customClient.user = 1
      customClient.save()

      nock(BASE)
        .post('/custom_logout_path/')
        .reply(200, {})

      return customClient.logOut().then(() => {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(customClient.token, null)
        assert.equal(stored.token, null)
        assert.equal(customClient.user, null)
        assert.equal(stored.user, null)
      })
    })

    it('should log user out', () => {
      nock(BASE)
        .post('/logout/')
        .reply(200, {})

      client.token = 'test'

      localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: 'test',
        user: 1
      }))

      return client.logOut().then(function () {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(client.token, null)
        assert.equal(stored.token, null)
        assert.equal(client.user, null)
        assert.equal(stored.user, null)
      })
    })

    it('should load stored configuration', (done) => {
      localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: 'ridiculous',
        user: 1
      }))

      client = new RESTClient(BASE)
      client.ready.then(() => {
        assert.equal(client.token, 'ridiculous')
        assert.equal(client.user, 1)
        done()
      })
    })

    it('should send token with authorized requests', () => {
      client.token = 'test'
    
      nock(BASE)
        .post('/logout/')
        .reply(200, function (req, body) {
          assert.equal(this.req.headers.authorization[0], 'Token test')
          return '{}'
        })

      return client.logOut()
    })

    it('should handle authorized request without token', () => {
      nock(BASE)
        .post('/logout/')
        .reply(401, {
          'detail': ''
        })

      return client.logOut().then((res) => {
        assert.fail('this should fail..')
      }).catch((err) => {
      })
    })

    it('should handle expired token', () => {
      var destroyHookCalled = false
      client.token = 'test'
      client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
        destroyHookCalled = true
      })

      nock(BASE)
        .post('/logout/')
        .reply(401, {
          'detail': ''
        })

      return client.logOut().then((res) => {
        assert.fail('this should fail..')
      }).catch((err) => {
        assert.equal(client.token, null)
        assert.equal(client.user, null)
        assert.equal(destroyHookCalled, false)
      })
    })
  })

  describe('oauth', () => {
    var oauth

    beforeEach(() => {
      oauth = client._oauth({
        route: '/oauth/'
      })
    })

    it('should handle failed login', () => {
      nock(BASE)
        .post('/oauth/')
        .reply(400, {})

      return oauth({
        code: 'test',
        redirect_uri: 'http://test.com'
      }).then((res) => {
        assert.fail('this should fail...')
      }).catch((err) => {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(err.status, 400)
        assert.equal(client.token, null)
        assert.equal(stored.token, null)
        assert.equal(client.user, null)
        assert.equal(stored.user, null)
      })
    })

    it('should handle successful login', () => {
      nock(BASE)
        .post('/oauth/')
        .reply(200, {
          token: 'test',
          user: 1
        })

      return oauth({
        code: 'test',
        redirect_uri: 'http://test.com'
      }).then((res) => {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY))
        assert.equal(client.token, 'test')
        assert.equal(stored.token, 'test')
        assert.equal(client.user, 1)
        assert.equal(stored.user, 1)
      })
    })
  })

  describe('resources', () => {
    var resource

    beforeEach(() => {
      resource = client._resource({
        route: 'test'
      })
    })

    describe('query', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test'
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .get('/test/')
          .reply(401, [])

        return resource.query().then((res) => {
          assert.fail('This should fail')
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null)
          assert.equal(client.user, null)
        })
      })

      it('should work', () => {
        var testResponse = [{'test': 'test'}]
        nock(BASE)
          .get('/test/')
          .reply(200, testResponse)

        return resource.query().then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })

      it('should work with params', () => {
        var testResponse = [{'test': 'test'}]
        var params = {expand: 'test.test', foo: 'bar'}

        nock(BASE)
          .get('/test/')
          .query(params)
          .reply(200, testResponse)

        return resource.query(params).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    })

    describe('get', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test'
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .get('/test/1/')
          .reply(401, {})

        return resource.get(1).then((res) => {
          assert.fail('This should fail')
        }).catch((err) => {
          assert.equal(client.token, null)
          assert.equal(client.user, null)
          assert.equal(test, true)
        })
      })

      it('should get', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .get('/test/1/')
          .reply(200, testResponse)

        return resource.get(1).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })

      it('should get with params', () => {
        var testResponse = {
          'test': 'test'
        }

        var params = {
          'expand': 'expanded',
          'foo': 'bar'
        }

        nock(BASE)
          .get('/test/1/')
          .query(params)
          .reply(200, testResponse)

        return resource.get(1, params).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    })

    describe('update', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test'
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .put('/test/1/')
          .reply(401, {})

        return resource.update(1).then((res) => {
          assert.fail('This should fail')
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null)
          assert.equal(client.user, null)
        })
      })

      it('should update', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .put('/test/1/', testResponse)
          .reply(200, testResponse)

        return resource.update(1, testResponse).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    })

    describe('create', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test'
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .post('/test/')
          .reply(401, {})

        return resource.create({}).then((res) => {
          assert.fail('This should fail')
        }).catch((err) => {
          assert.equal(client.token, null)
          assert.equal(client.user, null)
          assert.equal(test, true)
        })
      })

      it('should create', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .post('/test/', testResponse)
          .reply(200, testResponse)

        return resource.create(testResponse).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })

      it('should handle 204 no content responses', () => {
        var testData = {
          'test': 'test'
        }

        nock(BASE)
          .post('/test/', testData)
          .reply(204, undefined)

        return resource.create(testData).then((res) => {
          assert.deepEqual(res, {})
        })
      })
    })

    describe('delete', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test'
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .delete('/test/1/')
          .reply(401, {})

        return resource.delete(1).then((res) => {
          assert.fail('This should fail')
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null)
          assert.equal(client.user, null)
        })
      })

      it('should delete', () => {
        nock(BASE)
          .delete('/test/1/')
          .reply(204)

        return resource.delete(1).then((res) => {
          assert.deepEqual(res, undefined)
        })
      })
    })

    describe('detail', () => {
      beforeEach(() => {
        resource = client._resource({
          route: 'test',
          details: [
            { routeName: 'detail_get', method: 'get' },
            { routeName: 'detail_post', method: 'post' },
          ]
        })
      })

      it('should get', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/1/detail_get/')
          .reply(200, result)

        return resource.detail_get(1).then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/1/detail_get/?param1=1&param2=2')
          .reply(200, result)

        return resource.detail_get(1, { param1: 1, param2: 2 }).then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should handle 204 no content', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/1/detail_get/?param1=1&param2=2')
          .reply(204, result)

        return resource.detail_get(1, { param1: 1, param2: 2 }).then((res) => {
          assert.deepEqual(res, {})
        })
      })

      it.only('should post with params', () => {
        let result = { test: 'result' }
        let postBody = {
          param1: 1,
          param2: '2'
        }

        nock(BASE)
          .post('/test/1/detail_post/', postBody)
          .reply(200, result)

        return resource.detail_post(1, postBody).then((res) => {
          assert.deepEqual(res, result)
        })
      })
    })

    describe('list', () => {
      beforeEach(() => {
        resource = client._resource({
          route: 'test',
          lists: [
            { routeName: 'list_get', method: 'get' }
          ]
        })
      })

      it('should get', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/list_get/')
          .reply(200, result)

        return resource.list_get().then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/list_get/?param1=1&param2=2')
          .reply(200, result)

        return resource.list_get({
          param1: 1,
          param2: 2,
        }).then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with array params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/test/list_get/?param1=1&param2=2&param2=3')
          .reply(200, result)

        return resource.list_get({
          param1: 1,
          param2: [2, 3],
        }).then((res) => {
          assert.deepEqual(res, result)
        })
      })
    })

    describe('error', () => {
      it('should handle 400 bad request', () => {
        var res = [
          'error string',
          'error string',
        ]

        nock(BASE)
          .get('/test/1/')
          .reply(400, res)

        return resource.get(1).then((res) => {
          throw "this should have failed"
        }, (err) => {
          assert.deepEqual(err, res)
        })
      })

      it('should handle 404 not found', () => {
        var res = [
          'error string',
          'error string',
        ]

        nock(BASE)
          .get('/test/1/')
          .reply(404, res)

        return resource.get(1).then((res) => {
          throw "this should have failed"
        }, (err) => {
          assert.deepEqual(err, res)
        })
      })
    })
  })
})