'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'rest-client';
var DEFAULTS = {
  AUTH_PATH: '/token_auth/',
  LOGOUT_PATH: '/logout/'
};

var RESTClient = function () {
  function RESTClient(baseUrl, options) {
    _classCallCheck(this, RESTClient);

    if (!options) options = {};

    this.CONSTANTS = {
      AUTH_PATH: options.AUTH_PATH || DEFAULTS.AUTH_PATH,
      LOGOUT_PATH: options.LOGOUT_PATH || DEFAULTS.LOGOUT_PATH
    };

    this.base = baseUrl;
    this.token = null;
    this.events = {};
    this.ready = this.restore();
    this.EVENTS = {
      SESSION_EXPIRED: 'SESSION_EXPIRED',
      READY: 'READY'
    };
  }

  // this is a silly wrapper around localStorage so we can
  // work with asyncstorage in react native as well.
  // need to return a promise.


  _createClass(RESTClient, [{
    key: '_getItem',
    value: function _getItem(key) {
      return new Promise(function (resolve, reject) {
        var item = localStorage.getItem(key);
        if (item && item.then) {
          item.then(resolve);
        } else {
          resolve(item);
        }
      });
    }
  }, {
    key: '_setItem',
    value: function _setItem(key, value) {
      return new Promise(function (resolve, reject) {
        var res = localStorage.setItem(key, value);
        if (res && res.then) {
          res.then(resolve);
        } else {
          resolve(res);
        }
      });
    }
  }, {
    key: 'save',
    value: function save() {
      var _this = this;

      return new Promise(function (resolve, reject) {
        _this._setItem(LOCALSTORAGE_KEY, JSON.stringify({
          token: _this.token,
          user: _this.user
        })).then(resolve);
      });
    }
  }, {
    key: 'restore',
    value: function restore() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        _this2._getItem(LOCALSTORAGE_KEY).then(function (serialized) {
          var stored;
          if (serialized) {
            stored = JSON.parse(serialized);
            if (stored.token) {
              _this2.token = stored.token;
              _this2.user = stored.user;
            }
          }
          resolve();
        });
      });
    }
  }, {
    key: 'fetch',
    value: function (_fetch) {
      function fetch(_x, _x2) {
        return _fetch.apply(this, arguments);
      }

      fetch.toString = function () {
        return _fetch.toString();
      };

      return fetch;
    }(function (path, params) {
      var url = '' + this.base + path;
      var qsPos = url.indexOf('?');

      if (qsPos > -1) {
        if (url.substr(qsPos - 1, 1) !== '/') url = url.split('?').join('/?');
      } else {
        if (url.substr(url.length - 1) !== '/') url += '/';
      }

      if (!params.headers) params.headers = {};

      if (this.token) params.headers.authorization = 'Token ' + this.token;

      params.headers['Content-Type'] = 'application/json';
      params.headers.Accept = 'application/json';
      return fetch(url, params);
    })
  }, {
    key: 'logIn',
    value: function logIn(username, password) {
      var _this3 = this;

      var self = this;
      return new Promise(function (resolve, reject) {
        _this3.fetch(self.CONSTANTS.AUTH_PATH, {
          method: 'post',
          body: JSON.stringify({
            username: username,
            password: password
          })
        }).then(function (res) {
          if (res.status !== 200) reject(res);

          return res.json();
        }).then(function (body) {
          _this3.token = body.token;
          _this3.user = body.user;
          _this3.save();
          resolve();
        }).catch(function (err) {
          return reject(err);
        });
      });
    }
  }, {
    key: 'logOut',
    value: function logOut() {
      var _this4 = this;

      var self = this;
      return new Promise(function (resolve, reject) {
        if (_this4.token) {
          _this4.fetch(self.CONSTANTS.LOGOUT_PATH, {
            method: 'post'
          }).then(function (res) {
            _this4._destroySession();
            if (res.status !== 200) {
              reject(res.json());
            } else {
              resolve(res.json());
            }
          }).catch(function (err) {
            return reject(err);
          });
        } else {
          reject();
        }
      });
    }
  }, {
    key: 'addEventListener',
    value: function addEventListener(e, hook) {
      if (!this.events[e]) this.events[e] = [];

      this.events[e].push(hook);
    }
  }, {
    key: '_triggerEvent',
    value: function _triggerEvent(e) {
      if (this.events[e]) {
        this.events[e].forEach(function (hook) {
          hook();
        });
      }
    }
  }, {
    key: '_destroySession',
    value: function _destroySession() {
      this.token = null;
      this.user = null;
      this.save();
    }
  }, {
    key: '_oauth',
    value: function _oauth(config) {
      var self = this;
      return function (params) {
        var url = '' + config.route;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'post',
            body: JSON.stringify(params)
          }).then(function (res) {
            if (res.status !== 200) reject(res);

            return res.json();
          }).then(function (body) {
            self.token = body.token;
            self.user = body.user;
            self.save();
            resolve();
          }).catch(function (err) {
            return reject(err);
          });
        });
      };
    }
  }, {
    key: '_resource',
    value: function _resource(config) {
      var baseUrl = '/' + config.route + '/';
      var self = this;

      var encodeParams = function encodeParams(params) {
        var query;

        if (params) {
          query = '?' + Object.keys(params).map(function (key) {
            var arr = [];
            if (params[key] instanceof Array) {
              arr = params[key];
            } else {
              arr = [params[key]];
            }

            return arr.map(function (val) {
              return encodeURIComponent(key) + '=' + encodeURIComponent(val);
            }).join('&');
          }).join('&');
        } else {
          query = '';
        }

        return query;
      };

      var checkRes = function checkRes(res) {
        return new Promise(function (resolve, reject) {
          switch (res.status) {

            case 401:
              self._destroySession();
              self._triggerEvent(self.EVENTS.SESSION_EXPIRED);
              return reject("Session expired");

            case 400:
              return res.json().then(function (body) {
                return reject(body);
              });

            case 404:
              return res.json().then(function (body) {
                return reject(body);
              });

            default:
              return resolve(res);
          }
        });
      };

      var query = function query(params) {
        var url;
        var qs = encodeParams(params);
        url = '' + baseUrl + qs;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'get'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var get = function get(id, params) {
        var qs = encodeParams(params);
        var url = '' + baseUrl + id + '/' + qs;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'get'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var update = function update(id, data) {
        var url = '' + baseUrl + id + '/';
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'put',
            body: JSON.stringify(data)
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var create = function create(data) {
        var url = '' + baseUrl;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'post',
            body: JSON.stringify(data)
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            if (res.status === 204) {
              resolve({});
            } else {
              resolve(res.json());
            }
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var del = function del(id) {
        var url = '' + baseUrl + id + '/';
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'delete'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve();
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var methods = {
        query: query,
        get: get,
        update: update,
        create: create,
        delete: del
      };

      if (config.details) {
        config.details.forEach(function (detail) {
          methods[detail.routeName] = function (id, data) {
            var url = '' + baseUrl + id + '/' + detail.routeName + '/';
            var params = {
              method: detail.method
            };

            if (detail.method === 'get') {
              url += encodeParams(data);
            } else if (detail.method === 'post' || detail.method === 'put') {
              params.body = JSON.stringify(data);
            } else if (list.method === 'delete') {
              if (data) {
                url += encodeParams(data);
              }
            }

            return new Promise(function (resolve, reject) {
              self.fetch(url, params).then(function (res) {
                return checkRes(res);
              }).then(function (res) {
                if (res.status === 204) return resolve({});
                resolve(res.json());
              }).catch(function (err) {
                return reject(err);
              });
            });
          };
        });
      }

      if (config.lists) {
        config.lists.forEach(function (list) {
          methods[list.routeName] = function (data, urlParams) {
            var url = '' + baseUrl + list.routeName + '/';
            var params = {
              method: list.method
            };

            if (list.method === 'get') {
              url += encodeParams(data);
            } else if (list.method === 'post' || list.method === 'put') {
              params.body = JSON.stringify(data);

              if (urlParams) {
                url += encodeParams(urlParams);
              }
            } else if (list.method === 'delete') {
              if (data) {
                url += encodeParams(data);
              }
            }

            return new Promise(function (resolve, reject) {
              self.fetch(url, params).then(function (res) {
                return checkRes(res);
              }).then(function (res) {
                if (res.status === 204) return resolve({});
                resolve(res.json());
              }).catch(function (err) {
                return reject(err);
              });
            });
          };
        });
      }

      return methods;
    }
  }]);

  return RESTClient;
}();

exports.default = RESTClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
