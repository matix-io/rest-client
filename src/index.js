import es6Promise from 'es6-promise';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'rest-client';
const DEFAULTS = {
  AUTH_PATH: '/token_auth/',
  LOGOUT_PATH: '/logout/'
}

class RESTClient {
  constructor(baseUrl, options) {
    if (!options) 
      options = {}

    this.CONSTANTS = {
      AUTH_PATH: options.AUTH_PATH || DEFAULTS.AUTH_PATH,
      LOGOUT_PATH: options.LOGOUT_PATH || DEFAULTS.LOGOUT_PATH,
    }

    this.base = baseUrl;
    this.token = null;
    this.events = {};
    this.ready = this.restore();
    this.EVENTS = {
      SESSION_EXPIRED: 'SESSION_EXPIRED',
      READY: 'READY'
    }
  }

  // this is a silly wrapper around localStorage so we can
  // work with asyncstorage in react native as well.
  // need to return a promise.
  _getItem(key) {
    return new Promise((resolve, reject) => {
      var item = localStorage.getItem(key)
      if (item && item.then) {
        item.then(resolve)
      } else {
        resolve(item)
      }
    })
  }

  _setItem(key, value) {
    return new Promise((resolve, reject) => {
      var res = localStorage.setItem(key, value)
      if (res && res.then) {
        res.then(resolve)
      } else {
        resolve(res)
      }
    })
  }

  save() {
    return new Promise((resolve, reject) => {
      this._setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: this.token,
        user: this.user
      })).then(resolve)
    })
  }

  restore() {
    return new Promise((resolve, reject) => {
      this._getItem(LOCALSTORAGE_KEY).then((serialized) => {
        var stored;
        if (serialized) {
          stored = JSON.parse(serialized);
          if (stored.token) {
            this.token = stored.token;
            this.user = stored.user;
          }
        }
        resolve()
      })
    })
  }

  fetch(path, params) {
    var url = `${this.base}${path}`;
    var qsPos = url.indexOf('?')

    if (qsPos > -1) {
      if (url.substr(qsPos - 1, 1) !== '/')
        url = url.split('?').join('/?');
    } else {
      if (url.substr(url.length - 1) !== '/')
        url += '/';
    }

    if (!params.headers)
      params.headers = {};

    if (this.token)
      params.headers.authorization = `Token ${this.token}`;

    params.headers['Content-Type'] = 'application/json';
    params.headers.Accept = 'application/json';
    return fetch(url, params);
  }

  logIn(username, password) {
    let self = this
    return new Promise((resolve, reject) => {
      this.fetch(self.CONSTANTS.AUTH_PATH, {
        method: 'post',
        body: JSON.stringify({
          username,
          password
        })
      }).then((res) => {
        if (res.status !== 200)
          reject(res);

        return res.json();
      }).then((body) => {
        this.token = body.token;
        this.user = body.user;
        this.save();
        resolve();
      }).catch(err => reject(err));
    });
  }

  logOut() {
    let self = this
    return new Promise((resolve, reject) => {
      if (this.token) {
        this.fetch(self.CONSTANTS.LOGOUT_PATH, {
          method: 'post'
        }).then((res) => {
          this._destroySession();
          if (res.status !== 200) {
            reject(res.json());
          } else {
            resolve(res.json());
          }
        }).catch(err => reject(err));
      } else {
        reject();
      }
    });
  }

  addEventListener(e, hook) {
    if (!this.events[e])
      this.events[e] = []

    this.events[e].push(hook)
  }

  _triggerEvent(e) {
    if (this.events[e]) {
      this.events[e].forEach((hook) => {
        hook()
      })
    }
  }

  _destroySession() {
    this.token = null;
    this.user = null;
    this.save();
  }

  _oauth(config) {
    var self = this
    return function (params) {
      const url = `${config.route}`
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'post',
          body: JSON.stringify(params)
        }).then((res) => {
          if (res.status !== 200)
            reject(res)

          return res.json()
        }).then((body) => {
          self.token = body.token
          self.user = body.user
          self.save()
          resolve()
        }).catch((err) => reject(err))
      })
    }
  }

  _resource(config) {
    var baseUrl = `/${config.route}/`;
    var self = this;

    var encodeParams = function (params) {
      var query;

      if (params) {
        query = '?' + Object
          .keys(params)
          .map(key => {
            let arr = []
            if (params[key] instanceof Array) {
              arr = params[key]
            } else {
              arr = [ params[key] ]
            }

            return arr.map((val) => {
              return `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
            }).join('&')
          })
          .join('&')
      } else {
        query = '';
      }

      return query;
    }

    var checkRes = function (res) {
      return new Promise((resolve, reject) => {
        switch (res.status) {

        case 401:
          self._destroySession();
          self._triggerEvent(self.EVENTS.SESSION_EXPIRED)
          return reject("Session expired")

        case 400:
          return res.json().then((body) => reject(body))

        case 404:
          return res.json().then((body) => reject(body))

        default:
          return resolve(res)
        }
      })
    };

    var query = function (params) {
      var url
      var qs = encodeParams(params)
      url = `${baseUrl}${qs}`
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'get'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      });
    }

    var get = function (id, params) {
      var qs = encodeParams(params);
      var url = `${baseUrl}${id}/${qs}`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'get'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      })
    }

    var update = function (id, data) {
      var url = `${baseUrl}${id}/`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'put',
          body: JSON.stringify(data)
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      })
    }

    var create = function (data) {
      var url = `${baseUrl}`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'post',
          body: JSON.stringify(data)
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          if (res.status === 204) {
            resolve({})
          } else {
            resolve(res.json())
          }
        }).catch(err => reject(err))
      })
    }

    var del = function (id) {
      var url = `${baseUrl}${id}/`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'delete'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve()
        }).catch(err => reject(err))
      })
    }

    var methods = {
      query: query,
      get: get,
      update: update,
      create: create,
      delete: del
    }

    if (config.details) {
      config.details.forEach((detail) => {
        methods[detail.routeName] = function (id, data) {
          let url = `${baseUrl}${id}/${detail.routeName}/`
          let params = {
            method: detail.method
          }

          if (detail.method === 'get') {
            url += encodeParams(data)
          }

          else if (detail.method === 'post' || detail.method === 'put') {
            params.body = JSON.stringify(data)
          }

          else if (list.method === 'delete') {
            if (data) {
              url += encodeParams(data)
            }
          }

          return new Promise((resolve, reject) => {
            self.fetch(url, params).then((res) => {
              return checkRes(res)
            }).then((res) => {
              if (res.status === 204) return resolve({})
              resolve(res.json())
            }).catch(err => reject(err))
          })
        }
      })
    }

    if (config.lists) {
      config.lists.forEach((list) => {
        methods[list.routeName] = function (data, urlParams) {
          let url = `${baseUrl}${list.routeName}/`
          let params = {
            method: list.method
          }

          if (list.method === 'get') {
            url += encodeParams(data)
          }

          else if (list.method === 'post' || list.method === 'put') {
            params.body = JSON.stringify(data)

            if (urlParams) {
              url += encodeParams(urlParams)
            }
          }

          else if (list.method === 'delete') {
            if (data) {
              url += encodeParams(data)
            }
          }

          return new Promise((resolve, reject) => {
            self.fetch(url, params).then((res) => {
              return checkRes(res)
            }).then((res) => {
              if (res.status === 204) return resolve({})
              resolve(res.json())
            }).catch(err => reject(err))
          })
        }
      })
    }

    return methods
  }
}

export default RESTClient;

export { LOCALSTORAGE_KEY }